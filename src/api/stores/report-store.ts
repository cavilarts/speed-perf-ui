import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducer from '../reducers/rootReducer';

const loggerMiddleware = createLogger();

const configureStore = () => {
  return createStore(
    rootReducer,
    applyMiddleware(thunk, loggerMiddleware)
  );
}

export default configureStore;
