import { gql } from 'apollo-boost';

export const getReport = gql`
{
  reports {
    fistContentfulPaint,
    largestContentfulPaint,
    timeToInteractive,
    totalBlockingTime,
    cumulativeLayoutShift,
    speedIndex,
    score,
    date,
    site,
    device
  }
}
`;

export const addReportMutation = gql`
  mutation(
    $fistContentfulPaint: String!,
		$largestContentfulPaint: String!,
  	$timeToInteractive: String!,
		$totalBlockingTime: String!
  	$cumulativeLayoutShift: String!,
  	$speedIndex: String!,
    $score: String!,
    $time: String!,
    $site: String!,
    $device: String!,
  ) {
    addReport(
      fistContentfulPaint: $fistContentfulPaint,
      largestContentfulPaint: $largestContentfulPaint,
      timeToInteractive: $timeToInteractive,
      totalBlockingTime: $totalBlockingTime
      cumulativeLayoutShift: $cumulativeLayoutShift,
      speedIndex: $speedIndex,
      score: $score,
      time: $time,
      site: $site,
      device: $device
    ) {
      id
    }
  }
`;