import { GET_REPORT, RECEIVE_REPORT, CLEAN_REPORT } from '../constants/action-types';

const initialState = {
  report: [],
  site: null,
  error: false,
  isLoading: false
};

const reportReducer = (state = initialState, action:any) => {
  switch(action.type) {
    case GET_REPORT:
      return Object.assign({}, state, {
        isLoading: true
      });
    case RECEIVE_REPORT: {
      return Object.assign({}, state, {
        report: action.report,
        site: action.site,
        device: action.device,
        isLoading: false
      });
    }
    case CLEAN_REPORT: {
      return Object.assign({}, state, initialState);
    }
    default:
      return state;
  }
};

export default reportReducer;