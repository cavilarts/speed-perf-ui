import { combineReducers } from 'redux';

import reportReducer from './report-reducer';

export default combineReducers({
  reportReducer
})