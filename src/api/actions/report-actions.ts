import { GET_REPORT, RECEIVE_REPORT, CLEAN_REPORT } from '../constants/action-types';

const requestReport = () => ({
  type: GET_REPORT
});

const receiveReport = (response:any) => ({
  type: RECEIVE_REPORT,
  report: response.data,
  site: response.site,
  device: response.device
});

const cleanReport = () => ({
  type: CLEAN_REPORT
});

export function fetchReport (options:any) {
  return (dispatch:any) => {
    dispatch(requestReport());

    return fetch(
      'http://localhost:8080/api/v1/report/create',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(options)
      }
    )
      .then(response => {
        return response.json();
      })
      .then(response => {
        const avg = {
            time: response.data[response.data.length -1].time,
            fistContentfulPaint: (response.data.map((e:any) => parseFloat(e.fistContentfulPaint)).reduce((acc:any, cur:any) => acc + cur) / response.data.length).toFixed(1) + ' s',
            largestContentfulPaint: (response.data.map((e:any) => parseFloat(e.largestContentfulPaint)).reduce((acc:any, cur:any) => acc + cur) / response.data.length).toFixed(1) + ' s',
            speedIndex: (response.data.map((e:any) => parseFloat(e.speedIndex)).reduce((acc:any, cur:any) => acc + cur) / response.data.length).toFixed(1) + ' s',
            timeToInteractive: (response.data.map((e:any) => parseFloat(e.timeToInteractive)).reduce((acc:any, cur:any) => acc + cur) / response.data.length).toFixed(1) + ' s',
            totalBlockingTime: (response.data.map((e:any) => parseFloat(e.totalBlockingTime)).reduce((acc:any, cur:any) => acc + cur) / response.data.length).toFixed(1) + ' ms',
            cumulativeLayoutShift: (response.data.map((e:any) => parseFloat(e.cumulativeLayoutShift)).reduce((acc:any, cur:any) => acc + cur) / response.data.length).toFixed(0),
            score: (response.data.map((e:any) => parseFloat(e.score)).reduce((acc:any, cur:any) => acc + cur) / response.data.length).toFixed(2)
        };
        response.data.push(avg);
        return response;
      })
      .then(response => {
        return dispatch(receiveReport(response));
      })
      .catch(error => { console.error(error) });
  }
};

export function resetState () {
  return (dispatch:any) => {
    dispatch(cleanReport())
  }
}