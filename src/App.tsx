import React from 'react';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import './App.scss';
import configureStore from './api/stores/report-store';
import MainPage from './components/pages/main/main';
import CreateReport from './components/pages/create-report/create-report';
import ResultsPage from './components/pages/results/results';

const store = configureStore();
const client = new ApolloClient({
  uri: 'http://localhost:8080/graphql'
});

function App() {
  return (
    <main>
      <Router>
        <Switch>
          <Route path="/" component={MainPage} exact />
          <Provider store={store}>
            <Route path="/create-report" component={CreateReport} exact />
            <ApolloProvider client={client}>
              <Route path="/results" component={ResultsPage} />
            </ApolloProvider>
          </Provider>
        </Switch>
      </Router>
    </main>
  );
}

export default App;
