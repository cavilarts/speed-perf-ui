import React, { useState, useCallback } from 'react';

import './report-form.scss';
import {reportFormProps} from './report-form-interface';
import Button from '../../atoms/button/button';
import Icon from '../../atoms/icon/icon';
import UrlInput from '../../molecules/url-input/url-input';

const ReportForm:React.FC<reportFormProps> = (props) => {
  const {onSave} = props;
  const timesToRun = [1, 2 , 3, 4, 5, 6, 7, 8, 9, 10];
  const [url, setUrl] = useState('');
  const [times, setTimes] = useState(1);
  const [device, setDevice] = useState('mobile');
  const [throttling, setThrottling] = useState(false);
  const memoOnSave = useCallback(() => onSave({url, times, settings: {device, throttling}}), [url, times, device, throttling, onSave]);

  return(
    <section className="report-form">
      <div className="report-form--content">
        <h1 className="report-form--title">Which site you want to analyze</h1>
        <UrlInput
          onChange={(uri) => {
            setUrl(uri);
          }}
        />
      </div>
      <div className="report-form--content">
        <h2 className="report-form--title">Options to run your test</h2>
        <div className="report-form--field-group">
          <label htmlFor="times">Number of tests</label>
          <select name="times" id="times" onChange={e => {
            setTimes(+e.target.value);
          }}>
            {timesToRun.map((t, i) => <option key={i}>{t}</option>)}
          </select>
        </div>
        <div className="report-form--field-group">
          <label htmlFor="">Sleect device</label>
          <div className="report-form--button-option">
            <Button active={device === 'mobile'} onClick={e => setDevice('mobile')}><Icon name="mobile"/></Button>
          </div>
          <div className="report-form--button-option">
            <Button active={device === 'desktop'} onClick={e => setDevice('desktop')}><Icon name="laptop" /></Button>
          </div>
        </div>
        <div className="report-form--field-group">
          <label htmlFor="throttling">Throttling</label>
          <input type="checkbox" name="throttling" id="throttling" checked={throttling} onChange={e => setThrottling(!throttling)}/>
        </div>
      </div>
      <div>
        <Button onClick={(e) => memoOnSave()}>Analyze</Button>
      </div>
    </section>
  );
}

export default ReportForm;