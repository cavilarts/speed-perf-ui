import React from 'react';
import { useHistory } from 'react-router-dom';

import Divider from '../../atoms/divider/divider';
import Button from '../../atoms/button/button';
import Icon from '../../atoms/icon/icon';
import { Card } from '../../molecules/card/card';
import './actions.scss';

const Actions:React.FC = () => {
  const history = useHistory();

  return (
    <section className="actions">
      <Card className="actions--card">
        <Button className="actions--button" onClick={(e) => {
          history.push('/create-report')
        }}>
          <Icon name="play" />
          <p>Run A Test</p>
        </Button>
      </Card>
      <Divider direction="vertical">Or</Divider>
      <Card className="actions--card">
        <Button className="actions--button">
          <Icon name="clock" />
          <p>Historic Reports</p>
        </Button>
      </Card>
    </section>
  );
};

export default Actions;