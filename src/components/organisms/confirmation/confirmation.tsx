import React, { ReactElement } from 'react';

import Button from '../../atoms/button/button';
import { confirmationProps } from './confirmation-interface';
import './confirmation.scss';

const Confirmation:React.FC<confirmationProps> = (props) => {
  const { success } = props;

  return (
    <section>
      {success ? renderSuccess() : renderFail()}
    </section>
  );
};

const renderSuccess = ():ReactElement => {
  return (
    <div>
      <h2>Successful Upload!</h2>
      <p>Your test result were saved. Go and check them on.</p>
      <Button>Historic Reports</Button>
    </div>
  );
};

const renderFail = ():ReactElement => {
  return (
    <div>
      <div>
        <h2>Cannot Save The Report</h2>
        <p>There was an error trying to save your report.</p>
        <Button>Try Again</Button>
      </div>
    </div>
  );
};

export default Confirmation;