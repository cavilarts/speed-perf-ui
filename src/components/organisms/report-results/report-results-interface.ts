export interface reportResultsProps {
  report: metric[];
  resetState: any;
  saveReport: any;
  site: string;
  device: string;
};

export interface metric {
  time: string;
  fistContentfulPaint: string;
  largestContentfulPaint: string;
  speedIndex: string;
  timeToInteractive: string;
  totalBlockingTime: string;
  cumulativeLayoutShift: string;
  score: number;
};