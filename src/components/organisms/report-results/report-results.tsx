import React, { ReactElement } from 'react';

import Button from '../../atoms/button/button';
import { reportResultsProps } from './report-results-interface';
import './report-results.scss';


const ReportResults:React.FC<reportResultsProps> = (props) => {
  const { report, resetState, saveReport, site, device } = props;
  const avg = [...report].pop();

  return (
    <section>
      <h2>Test Results</h2>
      {renderResultsTable(report)}
      <Button onClick={() => {
        saveReport({
          variables: {...avg, site: site, device: device}
        })
      }}>Save Results</Button>
      <Button onClick={() => {
        resetState();
      }}>Run Another Test</Button>
    </section>
  );
};

const renderResultsTable = (results:any):ReactElement => {
  const report = [...results];
  const avg = report.pop();

  return (
    <table className="report-results">
      <thead>
        <tr>
          <th>Metrics</th>
          {report.map((r:any, i:number) => {
            return(
              <th key={i}>{`Test ${i + 1}`}</th>
            );
          })}
          <th>Average</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Cumulative Layout Shift</td>
          {report.map((r:any, i:number) => <td key={i}>{r.cumulativeLayoutShift}</td>)}
          <td>{avg.cumulativeLayoutShift}</td>
        </tr>
        <tr>
          <td>First Contentful Paint</td>
          {report.map((r:any, i:number) => <td key={i}>{r.fistContentfulPaint}</td>)}
          <td>{avg.fistContentfulPaint}</td>
        </tr>
        <tr>
          <td>Largest Contentful Paint</td>
          {report.map((r:any, i:number) => <td key={i}>{r.largestContentfulPaint}</td>)}
          <td>{avg.largestContentfulPaint}</td>
        </tr>
        <tr>
          <td>Speed Index</td>
          {report.map((r:any, i:number) => <td key={i}>{r.speedIndex}</td>)}
          <td>{avg.speedIndex}</td>
        </tr>
        <tr>
          <td>Time To Interactive</td>
          {report.map((r:any, i:number) => <td key={i}>{r.timeToInteractive}</td>)}
          <td>{avg.timeToInteractive}</td>
        </tr>
        <tr>
          <td>Total Blocking Time</td>
          {report.map((r:any, i:number) => <td key={i}>{r.totalBlockingTime}</td>)}
          <td>{avg.totalBlockingTime}</td>
        </tr>
        <tr>
          <td>Score</td>
          {report.map((r:any, i:number) => <td key={i}>{r.score}</td>)}
          <td>{avg.score}</td>
        </tr>
      </tbody>
    </table>
  );
}

export default ReportResults;