import { SyntheticEvent } from "react";

export interface urlInputProps {
  onChange: (url:string) => void;
}