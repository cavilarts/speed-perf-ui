import React, { useState, useCallback } from 'react';

import { urlInputProps } from './url-input-interface';
import './url-input.scss';

const UrlInput:React.FC<urlInputProps> = (props) => {
  const { onChange } = props;
  const [protocol, setProtocol] = useState('https://');
  const [resource, setResource] = useState('');
  const memoChange = useCallback<any>(
    (p:string, r:string) => onChange(p + r),
    [onChange]
  );

  return (
    <div className="url-input">
      <select 
        name="protocol"
        id="protocol"
        className="url-input--select"
        onChange={e => {
          setProtocol(e.target.value);
          memoChange(e.target.value, resource);
        }}
      >
        <option value="https://" defaultChecked>https://</option>
        <option value="http://" >http://</option>
      </select>
      <input
        type="text"
        name="resource"
        id="resource"
        value={resource}
        placeholder="www.yourwebsite.com"
        className="url-input--text"
        onChange={e => {
          setResource(e.target.value);
          memoChange(protocol, e.target.value);
        }}
      />

    </div>
  );
};

export default UrlInput;