import React from 'react';

import { cardProps, cardContentProps, cardDescriptionProps, cardHeaderProps } from './card-interface';
import './card.scss';

export const Card:React.FC<cardProps> = (props) => {
  const { children, className } = props;
  const classNames = `card ${className}`;

  return (
    <article className={classNames}>
      {children}
    </article>
  );
}

export const CardContent:React.FC<cardContentProps> = (props) => {
  const { children } = props;

  return (
    <div>
      {children}
    </div>
  );
};

export const CardHeader:React.FC<cardHeaderProps> = (props) => {
  const { children } = props;
  return (
    <h3>
      {children}
    </h3>
  );
};

export const CardDescription:React.FC<cardDescriptionProps> = (props) => {
  const { children } = props;
  return (
    <p>
      {children}
    </p>
  );
};
