import { ReactNode } from "react";

export interface cardProps {
  children?: ReactNode;
  className?: string
};

export interface cardContentProps {
  children?: ReactNode;
};

export interface cardHeaderProps {
  children?: ReactNode;
}

export interface cardDescriptionProps {
  children?: ReactNode;
};