import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useMutation } from 'react-apollo';

import { addReportMutation } from '../../../api/queries/addReport';
import { resultsPageProps } from './results-interface';
import { resetState } from '../../../api/actions/report-actions';
import ReportResults from '../../organisms/report-results/report-results';
import Confirmation from '../../organisms/confirmation/confirmation';
import './results.scss';


const ResultsPage:React.FC<resultsPageProps> = (props) => {
  const { history } = props;
  const content:any = useSelector(state => state);
  const dispatch = useDispatch();
  const reset = () => {
    dispatch(resetState());
  };
  const [saved, setSaved] = useState(false);
  const [errorSave, setErrorSave] = useState(false);
  const [saveReport] = useMutation(
    addReportMutation,
    {
      onCompleted() {
        setSaved(true);
      },
      onError() {
        setErrorSave(true)
      }
    }
  );
  const showResults = !content.reportReducer.isLoading && content.reportReducer.report.length && !saved && !errorSave;

  if (content.reportReducer.report && !content.reportReducer.report.length) {
    history.push('/create-report');
  }

  return (
    <>
      {
        showResults ?
          <ReportResults
            report={content.reportReducer.report}
            resetState={reset}
            saveReport={saveReport}
            site={content.reportReducer.site}
            device={content.reportReducer.device}
          /> :
          <Confirmation success={saved && !errorSave } />
      }
    </>
  );
};

export default ResultsPage;