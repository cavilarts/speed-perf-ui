import React, { ReactElement } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import ReportForm from '../../organisms/report-form/report-form';
import { createReportProps } from './create-report-interface';
import { fetchReport } from '../../../api/actions/report-actions';
import './create-report.scss';

const CreateReport:React.FC<createReportProps> = () => {
  const history = useHistory();
  const content:any = useSelector((state:any) => {
    if (!state.reportReducer.isLoading && state.reportReducer.report.length) {
      history.push('results');
    } else {
      return state;
    }
  });
  const dispatch = useDispatch();
  const report = content && content.reportReducer;

  return (
    <>
      {report && !report.isLoading && !report.report.length ? renderForm(dispatch) : null }
      {report && report.isLoading ? renderLoading() : null }
    </>
  )
};

const renderForm = (dispatch:any):ReactElement => {
  return (
    <ReportForm onSave={(settings) => {
      dispatch(fetchReport(settings));
    }}/>
  );
} 

const renderLoading = ():ReactElement => {
  return (
    <section>Loading...</section>
  );
};

export default CreateReport;