export interface createReportProps {
  error: boolean;
  report: report[],
  isLoading: boolean;
  fetchReportIfNeeded: any;
};

export interface report {
  time: string;
  fistContentfulPaint: string;
  largestContentfulPaint: string;
  speedIndex: string,
  timeToInteractive: string;
  totalBlockingTime: string,
  cumulativeLayoutShift: string;
  score: number;
}