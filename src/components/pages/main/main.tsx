import React from 'react';

import Actions from '../../organisms/actions/actions';

import './main.scss'

const MainPage:React.FC = () => {
  return (
    <section className="main-page">
      <h1 className="main-page--title">Page Speed Performance Report</h1>
      <Actions />
    </section>
  );
};

export default MainPage;
