export interface iconProps {
  circular?: boolean;
  name: 'home' | 'pencil' | 'image' | 'connection' | 'book' |
    'library' | 'file-text' | 'profile' | 'file-empty' | 'files-empty' | 
    'copy' | 'folder' | 'folder-open' | 'folder-plus' | 'folder-minus' |
    'coin-dollar' | 'coin-euro' | 'coin-pound' | 'location' | 'location-b' |
    'clock' | 'clock-b' | 'bell' | 'display' | 'laptop' | 'mobile' | 'mobile-b'|
    'floppy-disk' | 'database' | 'undo' | 'redo' | 'bubble' | 'bubbles' | 'user' |
    'users' | 'quotes-left' | 'quotes-right' | 'hour-glass' | 'spinner' | 'spinner-b' |
    'spinner-c' | 'search' | 'key' | 'cog' | 'cogs' | 'pie-chart' | 'stats-dots' | 'stats-bars' |
    'stats-bars-b' | 'meter' | 'meter-b' | 'lab' | 'bin' | 'menu' | 'cloud' | 'cloud-download' |
    'cloud-upload' | 'sphere' | 'link' | 'sun' | 'star-empty' | 'star-full' | 'warning' | 'notification' |
    'question' | 'cancel-circle' | 'blocked' | 'cross' | 'checkmark' | 'checkmark-b' | 'play' | 'pause' |
    'stop' | 'previous' | 'next' | 'backward' | 'forward' | 'circle-up' | 'circle-right' | 'circle-down' |
    'circle-left' | 'checkbox-checked' | 'checkbox-unchecked' | 'radio-checked' | 'radio-checked-b' |
    'filter' | 'table' | 'embed' | 'embed-b' | 'terminal' | 'google' | 'google-drive' | 'file-pdf' |
    'file-word' | 'file-excel';
};