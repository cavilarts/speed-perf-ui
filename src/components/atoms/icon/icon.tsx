import React from 'react';
import classNames from 'classnames';

import { iconProps } from './icon-interface';
import './icon.scss';

const Icon:React.FC<iconProps> = (props) => {
  const {name, circular} = props;
  const className = classNames(`icon-${name}`, {'icon--circular': circular});

  return <div className={className}></div>
}

export default Icon;