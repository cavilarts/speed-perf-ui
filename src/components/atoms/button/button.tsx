import React, { useCallback } from 'react';
import classNames from 'classnames';

import {buttonProps} from './button-interface';
import './button.scss';

const Button:React.FC<buttonProps> = (props) => {
  const { active, children, circular, disabled, onClick, className } = props;
  const memoClick = useCallback((e) => {
    if (onClick) {
      onClick(e)
    }
  },[onClick]);

  const classnames = classNames(
    'button',
    className,
    {'button--circular': !!circular},
    {'button__active': !!active}
  );

  return (
    <button disabled={disabled} className={classnames} onClick={memoClick}>
      {children ? children: null}
    </button>
  );
};

export default Button;
