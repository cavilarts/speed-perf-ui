import { SyntheticEvent, ReactNode } from "react";

export interface buttonProps {
  active?: boolean;
  children?: ReactNode;
  circular?: boolean;
  disabled?: boolean;
  onClick?: (e:SyntheticEvent) => any;
  className?: string;
}