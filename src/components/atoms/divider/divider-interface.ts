import { ReactNode } from "react";

export interface dividerProps {
  children: ReactNode;
  direction: 'vertical' | 'horizontal';
}