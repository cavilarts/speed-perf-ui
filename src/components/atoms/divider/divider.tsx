import React from 'react';

import { dividerProps } from './divider-interface';
import './divider.scss';

const Divider:React.FC<dividerProps> = (props) => {
  const {children, direction} = props;
  let classNames = 'divider';

  classNames += direction === 'horizontal' ? ' divider__horizontal' : ' divider__vertical'

  return (
    <div className={classNames}>
      <span className="text">{children}</span>
    </div>
  );
}

export default Divider;